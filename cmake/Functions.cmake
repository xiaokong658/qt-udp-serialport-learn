SET(_CSB_APP_PATH "bin")

set(_CSB_APP_TARGET "${CSB_ID}")

set(_CSB_LIBRARY_BASE_PATH "lib")
set(_CSB_LIBRARY_PATH "lib/${CSB_ID}")
set(_CSB_PLUGIN_PATH "lib/${CSB_ID}/plugins")
if (WIN32)
    set(_CSB_LIBEXEC_PATH "bin")
else ()
    set(_CSB_LIBEXEC_PATH "libexec/${CSB_ID}/bin")
endif ()
set(_CSB_DATA_PATH "share/${CSB_ID}")
set(_CSB_DOC_PATH "share/doc/${CSB_ID}")
set(_CSB_BIN_PATH "bin")
set(_CSB_LOG_PATH "log")

set(CSB_APP_PATH "${_CSB_APP_PATH}")                    # The target path of the SkyHawk application (relative to CMAKE_INSTALL_PREFIX).
set(CSB_APP_TARGET "${_CSB_APP_TARGET}")                # The SkyHawk application name.
set(CSB_PLUGIN_PATH "${_CSB_PLUGIN_PATH}")              # The SkyHawk plugin path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_LIBRARY_BASE_PATH "${_CSB_LIBRARY_BASE_PATH}")  # The SkyHawk library base path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_LIBRARY_PATH "${_CSB_LIBRARY_PATH}")            # The SkyHawk library path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_LIBEXEC_PATH "${_CSB_LIBEXEC_PATH}")            # The SkyHawk libexec path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_DATA_PATH "${_CSB_DATA_PATH}")                  # The SkyHawk data path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_DOC_PATH "${_CSB_DOC_PATH}")                    # The SkyHawk documentation path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_BIN_PATH "${_CSB_BIN_PATH}")                    # The SkyHawk bin path (relative to CMAKE_INSTALL_PREFIX).
set(CSB_LOG_PATH "${_CSB_LOG_PATH}")                    # The SkyHawk bin path (relative to CMAKE_INSTALL_PREFIX).

file(RELATIVE_PATH RELATIVE_PLUGIN_PATH "/${CSB_BIN_PATH}" "/${CSB_PLUGIN_PATH}")
file(RELATIVE_PATH RELATIVE_LIBEXEC_PATH "/${CSB_BIN_PATH}" "/${CSB_LIBEXEC_PATH}")
file(RELATIVE_PATH RELATIVE_DATA_PATH "/${CSB_BIN_PATH}" "/${CSB_DATA_PATH}")
file(RELATIVE_PATH RELATIVE_DOC_PATH "/${CSB_BIN_PATH}" "/${CSB_DOC_PATH}")
file(RELATIVE_PATH RELATIVE_LOG_PATH "/${CSB_BIN_PATH}" "/${CSB_LOG_PATH}")

list(APPEND DEFAULT_DEFINES
        RELATIVE_PLUGIN_PATH="${RELATIVE_PLUGIN_PATH}"
        RELATIVE_LIBEXEC_PATH="${RELATIVE_LIBEXEC_PATH}"
        RELATIVE_DATA_PATH="${RELATIVE_DATA_PATH}"
        RELATIVE_DOC_PATH="${RELATIVE_DOC_PATH}"
        RELATIVE_LOG_PATH="${RELATIVE_LOG_PATH}"
        )

file(RELATIVE_PATH _PLUGIN_TO_LIB "/${CSB_PLUGIN_PATH}" "/${CSB_LIBRARY_PATH}")

if (APPLE)
    set(_RPATH_BASE "@executable_path")
    set(_LIB_RPATH "@loader_path")
    set(_PLUGIN_RPATH "@loader_path;@loader_path/${_PLUGIN_TO_LIB}")
elseif (WIN32)
    set(_RPATH_BASE "")
    set(_LIB_RPATH "")
    set(_PLUGIN_RPATH "")
else()
    set(_RPATH_BASE "\$ORIGIN")
    set(_LIB_RPATH "\$ORIGIN")
    set(_PLUGIN_RPATH "\$ORIGIN;\$ORIGIN/${_PLUGIN_TO_LIB}")
endif ()

set(__CSB_PLUGINS "" CACHE INTERNAL "*** Internal ***")
set(__CSB_LIBRARIES "" CACHE INTERNAL "*** Internal ***")
set(__CSB_EXECUTABLES "" CACHE INTERNAL "*** Internal ***")
set(__CSB_TESTS "" CACHE INTERNAL "*** Internal ***")

#
# Internal functions
#

function(append_extra_translations target_name)
    if(NOT ARGN)
        return()
    endif()

    if(TARGET "${target_name}")
        get_target_property(_input "${target_name}" QT_EXTRA_TRANSLATIONS)
        if (_input)
            set(_output "${_input}" "${ARGN}")
        else()
            set(_output "${ARGN}")
        endif()
        set_target_properties("${target_name}" PROPERTIES QT_EXTRA_TRANSLATIONS "${_output}")
    endif()
endfunction()

function(update_cached_list name value)
    set(_tmp_list "${${name}}")
    list(APPEND _tmp_list "${value}")
    set("${name}" "${_tmp_list}" CACHE INTERNAL "*** Internal ***")
endfunction()

function(compare_sources_with_existing_disk_files target_name sources)
    if(NOT WITH_DEBUG_CMAKE)
        return()
    endif()

    file(GLOB_RECURSE existing_files RELATIVE ${CMAKE_CURRENT_LIST_DIR} "*.cpp" "*.hpp" "*.c" "*.h" "*.ui" "*.qrc")
    foreach(file IN LISTS existing_files)
        if(NOT ${file} IN_LIST sources)
            if (NOT WITH_TESTS AND ${file} MATCHES "test")
                continue()
            endif()
            message(STATUS "${target_name} doesn't include ${file}")
        endif()
    endforeach()

    foreach(source IN LISTS "${sources}")
        if(NOT ${source} IN_LIST existing_files)
            if (NOT WITH_TESTS AND ${file} MATCHES "test")
                continue()
            endif()
            message(STATUS "${target_name} contains non existing ${source}")
        endif()
    endforeach()
endfunction(compare_sources_with_existing_disk_files)

function(separate_object_libraries libraries REGULAR_LIBS OBJECT_LIBS OBJECT_LIB_OBJECTS)
    if (CMAKE_VERSION VERSION_LESS 3.14)
        foreach(lib IN LISTS libraries)
            if (TARGET ${lib})
                get_target_property(lib_type ${lib} TYPE)
                if (lib_type STREQUAL "OBJECT_LIBRARY")
                    list(APPEND object_libs ${lib})
                    list(APPEND object_libs_objects $<TARGET_OBJECTS:${lib}>)
                else()
                    list(APPEND regular_libs ${lib})
                endif()
            else()
                list(APPEND regular_libs ${lib})
            endif()
            set(${REGULAR_LIBS} ${regular_libs} PARENT_SCOPE)
            set(${OBJECT_LIBS} ${object_libs} PARENT_SCOPE)
            set(${OBJECT_LIB_OBJECTS} ${object_libs_objects} PARENT_SCOPE)
        endforeach()
    else()
        set(${REGULAR_LIBS} ${libraries} PARENT_SCOPE)
        unset(${OBJECT_LIBS} PARENT_SCOPE)
        unset(${OBJECT_LIB_OBJECTS} PARENT_SCOPE)
    endif()
endfunction(separate_object_libraries)

function(set_explicit_moc target_name file)
    unset(file_dependencies)
    if (file MATCHES "^.*plugin.h$")
        set(file_dependencies DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/${target_name}.json")
    endif()
    set_property(SOURCE "${file}" PROPERTY SKIP_AUTOMOC ON)
    qt5_wrap_cpp(file_moc "${file}" ${file_dependencies})
    target_sources(${target_name} PRIVATE "${file_moc}")
endfunction()

function(set_public_headers target sources)
    foreach(source IN LISTS sources)
        if (source MATCHES "\.h$|\.hpp$")
            if (NOT IS_ABSOLUTE ${source})
                set(source "${CMAKE_CURRENT_SOURCE_DIR}/${source}")
            endif()
            get_filename_component(source_dir ${source} DIRECTORY)
            file(RELATIVE_PATH include_dir_relative_path ${PROJECT_SOURCE_DIR} ${source_dir})
            install(
                    FILES ${source}
                    DESTINATION "include/${include_dir_relative_path}"
                    COMPONENT Devel EXCLUDE_FROM_ALL)
        endif()
    endforeach()
endfunction()

function(set_public_includes target includes)
    foreach(inc_dir IN LISTS includes)
        if (NOT IS_ABSOLUTE ${inc_dir})
            set(inc_dir "${CMAKE_CURRENT_SOURCE_DIR}/${inc_dir}")
        endif()
        target_include_directories(${target} PUBLIC $<BUILD_INTERFACE:${inc_dir}>)
    endforeach()
endfunction()

function(finalize_test_setup test_name)
    # Never translate tests:
    set_tests_properties(${name} PROPERTIES QT_SKIP_TRANSLATION ON)

    if (WIN32)
        list(APPEND env_path $ENV{PATH})
        list(APPEND env_path ${CMAKE_BINARY_DIR}/${CSB_PLUGIN_PATH})
        list(APPEND env_path ${CMAKE_BINARY_DIR}/${CSB_BIN_PATH})
        list(APPEND env_path $<TARGET_FILE_DIR:Qt5::Test>)
        if (TARGET libclang)
            list(APPEND env_path $<TARGET_FILE_DIR:libclang>)
        endif()

        string(REPLACE "/" "\\" env_path "${env_path}")
        string(REPLACE ";" "\\;" env_path "${env_path}")

        set_tests_properties(${test_name} PROPERTIES ENVIRONMENT "PATH=${env_path}")
    endif()
endfunction()

function(csb_add_depends target_name)
    cmake_parse_arguments(_arg "" "" "PRIVATE;PUBLIC" ${ARGN})
    if (${_arg_UNPARSED_ARGUMENTS})
        message(FATAL_ERROR "csb_add_depends had unparsed arguments")
    endif()

    separate_object_libraries("${_arg_PRIVATE}" depends object_lib_depends object_lib_depends_objects)
    separate_object_libraries("${_arg_PUBLIC}" public_depends object_public_depends object_public_depends_objects)

    target_sources(${target_name} PRIVATE ${object_lib_depends_objects} ${object_public_depends_objects})

    get_target_property(target_type ${target_name} TYPE)
    if (NOT target_type STREQUAL "OBJECT_LIBRARY")
        target_link_libraries(${target_name} PRIVATE ${depends} PUBLIC ${public_depends})
    else()
        list(APPEND object_lib_depends ${depends})
        list(APPEND object_public_depends ${public_depends})
    endif()

    foreach(obj_lib IN LISTS object_lib_depends)
        target_compile_definitions(${target_name} PRIVATE $<TARGET_PROPERTY:${obj_lib},INTERFACE_COMPILE_DEFINITIONS>)
        target_include_directories(${target_name} PRIVATE $<TARGET_PROPERTY:${obj_lib},INTERFACE_INCLUDE_DIRECTORIES>)
    endforeach()
    foreach(obj_lib IN LISTS object_public_depends)
        target_compile_definitions(${target_name} PUBLIC $<TARGET_PROPERTY:${obj_lib},INTERFACE_COMPILE_DEFINITIONS>)
        target_include_directories(${target_name} PUBLIC $<TARGET_PROPERTY:${obj_lib},INTERFACE_INCLUDE_DIRECTORIES>)
    endforeach()
endfunction()

function(find_dependent_plugins varName)
    set(_RESULT ${ARGN})
    foreach(i ${ARGN})
        get_property(_dep TARGET "${i}" PROPERTY _arg_DEPENDS)
        if (_dep)
            find_dependent_plugins(_REC ${_dep})
            list(APPEND _RESULT ${_REC})
        endif()
    endforeach()
    if (_RESULT)
        list(REMOVE_DUPLICATES _RESULT)
        list(SORT _RESULT)
    endif()

    set("${varName}" ${_RESULT} PARENT_SCOPE)
endfunction()

function(csb_plugin_enabled varName name)
    if (NOT (name IN_LIST __CSB_PLUGINS))
        message(FATAL_ERROR "csb_extend_plugin: Unknown plugin target \"${name}\"")
    endif()
    if (TARGET ${name})
        set(${varName} ON PARENT_SCOPE)
    else()
        set(${varName} OFF PARENT_SCOPE)
    endif()
endfunction()

function(enable_pch target)
    if (BUILD_WITH_PCH)
        # Skip PCH for targets that do not use the expected visibility settings:
        get_target_property(visibility_property "${target}" CXX_VISIBILITY_PRESET)
        get_target_property(inlines_property "${target}" VISIBILITY_INLINES_HIDDEN)

        if (NOT visibility_property STREQUAL "hidden" OR NOT inlines_property)
            return()
        endif()

        get_target_property(target_type ${target} TYPE)
        if (NOT ${target_type} STREQUAL "OBJECT_LIBRARY")
            function(_recursively_collect_dependencies input_target)
                get_target_property(input_type ${input_target} TYPE)
                if (${input_type} STREQUAL "INTERFACE_LIBRARY")
                    set(prefix "INTERFACE_")
                endif()
                get_target_property(link_libraries ${input_target} ${prefix}LINK_LIBRARIES)
                foreach(library IN LISTS link_libraries)
                    if(TARGET ${library} AND NOT ${library} IN_LIST dependencies)
                        list(APPEND dependencies ${library})
                        _recursively_collect_dependencies(${library})
                    endif()
                endforeach()
                set(dependencies ${dependencies} PARENT_SCOPE)
            endfunction()
            _recursively_collect_dependencies(${target})

            function(_add_pch_target pch_target pch_file pch_dependency)
                if (EXISTS ${pch_file})
                    add_library(${pch_target} STATIC
                            ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.cpp
                            ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.c)
                    target_compile_definitions(${pch_target} PRIVATE ${DEFAULT_DEFINES})
                    set_target_properties(${pch_target} PROPERTIES
                            PRECOMPILE_HEADERS ${pch_file}
                            CXX_VISIBILITY_PRESET hidden
                            VISIBILITY_INLINES_HIDDEN ON)
                    target_link_libraries(${pch_target} PRIVATE ${pch_dependency})
                endif()
            endfunction()

            if (NOT TARGET SkyHawkPchGui AND NOT TARGET SkyHawkPchConsole)
                file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.c_cpp.in "/*empty file*/")
                configure_file(
                        ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.c_cpp.in
                        ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.cpp)
                configure_file(
                        ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.c_cpp.in
                        ${CMAKE_CURRENT_BINARY_DIR}/empty_pch.c)

                _add_pch_target(SkyHawkPchGui
                        "${PROJECT_SOURCE_DIR}/src/shared/csb_gui_pch.h" Qt5::Widgets)
                _add_pch_target(SkyHawkPchConsole
                        "${PROJECT_SOURCE_DIR}/src/shared/csb_pch.h" Qt5::Core)
            endif()

            unset(PCH_TARGET)
            if ("Qt5::Widgets" IN_LIST dependencies)
                set(PCH_TARGET SkyHawkPchGui)
            elseif ("Qt5::Core" IN_LIST dependencies)
                set(PCH_TARGET SkyHawkPchConsole)
            endif()

            if (TARGET "${PCH_TARGET}")
                set_target_properties(${target} PROPERTIES
                        PRECOMPILE_HEADERS_REUSE_FROM ${PCH_TARGET})
            endif()
        endif()
    endif()
endfunction()

function(csb_output_binary_dir varName)
    if (CSB_MERGE_BINARY_DIR)
        set(${varName} ${SkyHawk_BINARY_DIR} PARENT_SCOPE)
    else()
        set(${varName} ${PROJECT_BINARY_DIR} PARENT_SCOPE)
    endif()
endfunction()

function(condition_info varName condition)
    if (NOT ${condition})
        set(${varName} "" PARENT_SCOPE)
    else()
        string(REPLACE ";" " " _contents "${${condition}}")
        set(${varName} "with CONDITION ${_contents}" PARENT_SCOPE)
    endif()
endfunction()

#
# Public API functions
#
function(csb_add_library name)
    cmake_parse_arguments(_arg
            "STATIC;OBJECT;SKIP_TRANSLATION;BUILD_BY_DEFAULT"
            "DESTINATION"
            "LIBPATH;DEFINES;DEPENDS;EXTRA_TRANSLATIONS;INCLUDES;PUBLIC_DEFINES;PUBLIC_DEPENDS;PUBLIC_INCLUDES;SOURCES;EXPLICIT_MOC;SKIP_AUTOMOC;PROPERTIES"
            ${ARGN})

    if (${_arg_UNPARSED_ARGUMENTS})
        message(FATAL_ERROR "csb_add_library had unparsed arguments")
    endif()

    update_cached_list(__CSB_LIBRARIES "${name}")

    compare_sources_with_existing_disk_files(${name} "${_arg_SOURCES}")

    set(library_type SHARED)
    if (_arg_STATIC)
        set(library_type STATIC)
    endif()
    if (_arg_OBJECT)
        set(library_type OBJECT)
    endif()

    set(_exclude_from_all EXCLUDE_FROM_ALL)
    if (_arg_BUILD_BY_DEFAULT)
        unset(_exclude_from_all)
    endif()

    foreach(_arg_SOURCE IN ITEMS ${_arg_SOURCES})
        IF (NOT EXISTS ${_arg_SOURCE})
            MESSAGE(FATAL_ERROR "TARGET: [${name}], Not Found: `${_arg_SOURCE}`")
        ENDIF()
    endforeach()

    add_library(${name} ${library_type} ${_exclude_from_all} ${_arg_SOURCES})
    add_library(${CSB_CASED_ID}::${name} ALIAS ${name})
    set_public_headers(${name} "${_arg_SOURCES}")

    if (${name} MATCHES "^[^0-9-]+$")
        string(TOUPPER "${name}_LIBRARY" EXPORT_SYMBOL)
    endif()

    if (WITH_TESTS)
        set(TEST_DEFINES WITH_TESTS SRCDIR="${CMAKE_CURRENT_SOURCE_DIR}")
    endif()

    file(RELATIVE_PATH include_dir_relative_path ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

    if (_arg_LIBPATH)
        target_link_directories(${name} PRIVATE ${_arg_LIBPATH})
    endif(_arg_LIBPATH)

    target_include_directories(${name}
            PRIVATE ${_arg_INCLUDES}
            PUBLIC
            "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
            "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>"
            "$<INSTALL_INTERFACE:include/${include_dir_relative_path}>"
            "$<INSTALL_INTERFACE:include/${include_dir_relative_path}/..>")

    set_public_includes(${name} "${_arg_PUBLIC_INCLUDES}")

    target_compile_definitions(${name}
            PRIVATE ${EXPORT_SYMBOL} ${DEFAULT_DEFINES} ${_arg_DEFINES} ${TEST_DEFINES}
            PUBLIC ${_arg_PUBLIC_DEFINES})

    csb_add_depends(${name}
            PRIVATE ${_arg_DEPENDS} ${IMPLICIT_DEPENDS}
            PUBLIC ${_arg_PUBLIC_DEPENDS})

    foreach(file IN LISTS _arg_EXPLICIT_MOC)
        set_explicit_moc(${name} "${file}")
    endforeach()

    foreach(file IN LISTS _arg_SKIP_AUTOMOC)
        set_property(SOURCE ${file} PROPERTY SKIP_AUTOMOC ON)
    endforeach()

    set(skip_translation OFF)
    if (_arg_SKIP_TRANSLATION)
        set(skip_translation ON)
    endif()

    set(_DESTINATION "${CSB_BIN_PATH}")
    if (_arg_DESTINATION)
        set(_DESTINATION "${_arg_DESTINATION}")
    endif()

    csb_output_binary_dir(_output_binary_dir)
    set_target_properties(${name} PROPERTIES
            SOURCES_DIR "${CMAKE_CURRENT_SOURCE_DIR}"
            VERSION "${CSB_VERSION}"
            CXX_VISIBILITY_PRESET hidden
            VISIBILITY_INLINES_HIDDEN ON
            BUILD_RPATH "${_LIB_RPATH}"
            INSTALL_RPATH "${_LIB_RPATH}"
            RUNTIME_OUTPUT_DIRECTORY "${_output_binary_dir}/${_DESTINATION}"
            LIBRARY_OUTPUT_DIRECTORY "${_output_binary_dir}/${CSB_LIBRARY_PATH}"
            ARCHIVE_OUTPUT_DIRECTORY "${_output_binary_dir}/${CSB_LIBRARY_PATH}"
            ${_arg_PROPERTIES})

    enable_pch(${name})

    unset(NAMELINK_OPTION)
    if (library_type STREQUAL "SHARED")
        set(NAMELINK_OPTION NAMELINK_SKIP)
    endif()

    install(TARGETS ${name}
            EXPORT ${CSB_CASED_ID}
            RUNTIME DESTINATION "${_DESTINATION}" OPTIONAL
            LIBRARY
            DESTINATION "${CSB_LIBRARY_PATH}"
            ${NAMELINK_OPTION}
            OPTIONAL
            OBJECTS
            DESTINATION "${CSB_LIBRARY_PATH}"
            COMPONENT Devel EXCLUDE_FROM_ALL
            ARCHIVE
            DESTINATION "${CSB_LIBRARY_PATH}"
            COMPONENT Devel EXCLUDE_FROM_ALL
            OPTIONAL)

    if (NAMELINK_OPTION)
        install(TARGETS ${name}
                LIBRARY
                DESTINATION "${CSB_LIBRARY_PATH}"
                NAMELINK_ONLY
                COMPONENT Devel EXCLUDE_FROM_ALL
                OPTIONAL
                )
    endif()

    append_extra_translations("${name}" "${_arg_EXTRA_TRANSLATIONS}")
endfunction(csb_add_library)

function(csb_add_plugin target_name)
    cmake_parse_arguments(_arg
            "EXPERIMENTAL;SKIP_DEBUG_CMAKE_FILE_CHECK;SKIP_INSTALL;INTERNAL_ONLY;SKIP_TRANSLATION"
            "VERSION;COMPAT_VERSION;PLUGIN_JSON_IN;PLUGIN_PATH;PLUGIN_NAME;OUTPUT_NAME"
            "LIBPATH;CONDITION;DEPENDS;EXTRA_TRANSLATIONS;PUBLIC_DEPENDS;DEFINES;PUBLIC_DEFINES;INCLUDES;PUBLIC_INCLUDES;PLUGIN_DEPENDS;PLUGIN_RECOMMENDS;SOURCES;EXPLICIT_MOC"
            ${ARGN}
            )

    if (${_arg_UNPARSED_ARGUMENTS})
        message(FATAL_ERROR "csb_add_plugin had unparsed arguments")
    endif()

    update_cached_list(__CSB_PLUGINS "${target_name}")

    set(name ${target_name})
    if (_arg_PLUGIN_NAME)
        set(name ${_arg_PLUGIN_NAME})
    endif()

    condition_info(_extra_text _arg_CONDITION)
    if (NOT _arg_CONDITION)
        set(_arg_CONDITION ON)
    endif()

    string(TOUPPER "BUILD_PLUGIN_${target_name}" _build_plugin_var)
    set(_build_plugin_default "ON")
    if (DEFINED ENV{CSB_${_build_plugin_var}})
        set(_build_plugin_default "$ENV{CSB_${_build_plugin_var}}")
    endif()
    if (_arg_INTERNAL_ONLY)
        set(${_build_plugin_var} "${_build_plugin_default}")
    else()
        set(${_build_plugin_var} "${_build_plugin_default}" CACHE BOOL "Build plugin ${name}.")
    endif()

    if ((${_arg_CONDITION}) AND ${_build_plugin_var})
        set(_plugin_enabled ON)
    else()
        set(_plugin_enabled OFF)
    endif()

    if (NOT _arg_INTERNAL_ONLY)
        add_feature_info("Plugin ${name}" _plugin_enabled "${_extra_text}")
    endif()
    if (NOT _plugin_enabled)
        return()
    endif()

    ### Generate plugin.json file:
    if (NOT _arg_VERSION)
        set(_arg_VERSION ${CSB_VERSION})
    endif()
    if (NOT _arg_COMPAT_VERSION)
        set(_arg_COMPAT_VERSION ${_arg_VERSION})
    endif()

    if (NOT _arg_SKIP_DEBUG_CMAKE_FILE_CHECK)
        compare_sources_with_existing_disk_files(${target_name} "${_arg_SOURCES}")
    endif()

    # Generate dependency list:
    find_dependent_plugins(_DEP_PLUGINS ${_arg_PLUGIN_DEPENDS})

    set(_arg_DEPENDENCY_STRING "\"Dependencies\": [\n")
    foreach(i IN LISTS _DEP_PLUGINS)
        if (i MATCHES "^${CSB_CASED_ID}::")
            set(_v ${CSB_VERSION})
            string(REPLACE "${CSB_CASED_ID}::" "" i ${i})
        else()
            get_property(_v TARGET "${i}" PROPERTY _arg_VERSION)
        endif()
        string(APPEND _arg_DEPENDENCY_STRING
                "        { \"Name\": \"${i}\", \"Version\": \"${_v}\" }")
    endforeach(i)
    string(REPLACE "}        {" "},\n        {"
            _arg_DEPENDENCY_STRING "${_arg_DEPENDENCY_STRING}")
    foreach(i IN LISTS ${_arg_RECOMMENDS})
        if (i MATCHES "^${CSB_CASED_ID}::")
            set(_v ${CSB_VERSION})
            string(REPLACE "${CSB_CASED_ID}::" "" i ${i})
        else()
            get_property(_v TARGET "${i}" PROPERTY _arg_VERSION)
        endif()
        string(APPEND _arg_DEPENDENCY_STRING
                "        { \"Name\": \"${i}\", \"Version\": \"${_v}\", \"Type\": \"optional\" }")
    endforeach(i)

    string(APPEND _arg_DEPENDENCY_STRING "\n    ]")
    if (_arg_EXPERIMENTAL)
        string(APPEND _arg_DEPENDENCY_STRING ",\n    \"Experimental\": true")
    endif()

    set(CSB_PLUGIN_DEPENDENCY_STRING ${_arg_DEPENDENCY_STRING})

    ### 插件描述文件 [name].json.in

    if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${name}.json.in")
        file(READ "${name}.json.in" plugin_json_in)
        string(REPLACE "\\\"" "\"" plugin_json_in ${plugin_json_in})
        string(REPLACE "\\'" "'" plugin_json_in ${plugin_json_in})
        string(REPLACE "$$CSB_VERSION" "\${CSB_VERSION}" plugin_json_in ${plugin_json_in})
        string(REPLACE "$$CSB_COMPAT_VERSION" "\${CSB_VERSION_COMPAT}" plugin_json_in ${plugin_json_in})
        string(REPLACE "$$CSB_COPYRIGHT_YEAR" "\${CSB_COPYRIGHT_YEAR}" plugin_json_in ${plugin_json_in})
        string(REPLACE "$$dependencyList" "\${CSB_PLUGIN_DEPENDENCY_STRING}" plugin_json_in ${plugin_json_in})
        if(_arg_PLUGIN_JSON_IN)
            #e.g. UPDATEINFO_EXPERIMENTAL_STR=true
            string(REGEX REPLACE "=.*$" "" json_key ${_arg_PLUGIN_JSON_IN})
            string(REGEX REPLACE "^.*=" "" json_value ${_arg_PLUGIN_JSON_IN})
            string(REPLACE "$$${json_key}" "${json_value}" plugin_json_in ${plugin_json_in})
        endif()
        file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${name}.json.cmakein" ${plugin_json_in})

        configure_file("${CMAKE_CURRENT_BINARY_DIR}/${name}.json.cmakein" "${name}.json")
    endif()
    add_library(${target_name} SHARED ${_arg_SOURCES})
    add_library(${CSB_CASED_ID}::${target_name} ALIAS ${target_name})
    set_public_headers(${target_name} "${_arg_SOURCES}")

    ### Generate EXPORT_SYMBOL
    string(TOUPPER "${name}_LIBRARY" EXPORT_SYMBOL)

    if (WITH_TESTS)
        set(TEST_DEFINES WITH_TESTS SRCDIR="${CMAKE_CURRENT_SOURCE_DIR}")
    endif()

    file(RELATIVE_PATH include_dir_relative_path ${PROJECT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

    target_link_directories(${target_name}
            PRIVATE ${_arg_LIBPATH})

    target_include_directories(${target_name}
            PRIVATE
            ${_arg_INCLUDES}
            "${CMAKE_CURRENT_BINARY_DIR}"
            "${CMAKE_BINARY_DIR}/src"
            PUBLIC
            "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>"
            "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..>"
            "$<INSTALL_INTERFACE:include/${include_dir_relative_path}>"
            "$<INSTALL_INTERFACE:include/${include_dir_relative_path}/..>"
            )
    set_public_includes(${target_name} "${_arg_PUBLIC_INCLUDES}")

    target_compile_definitions(${target_name}
            PRIVATE ${EXPORT_SYMBOL} ${DEFAULT_DEFINES} ${_arg_DEFINES} ${TEST_DEFINES}
            PUBLIC ${_arg_PUBLIC_DEFINES}
            )

    csb_add_depends(${target_name}
            PRIVATE ${_arg_DEPENDS} ${_DEP_PLUGINS} ${IMPLICIT_DEPENDS}
            PUBLIC ${_arg_PUBLIC_DEPENDS}
            )

    set(plugin_dir "${CSB_PLUGIN_PATH}")
    if (_arg_PLUGIN_PATH)
        set(plugin_dir "${_arg_PLUGIN_PATH}")
    endif()

    set(skip_translation OFF)
    if (_arg_SKIP_TRANSLATION)
        set(skip_translation ON)
    endif()

    csb_output_binary_dir(_output_binary_dir)
    set_target_properties(${target_name} PROPERTIES
            SOURCES_DIR "${CMAKE_CURRENT_SOURCE_DIR}"
            CXX_VISIBILITY_PRESET hidden
            VISIBILITY_INLINES_HIDDEN ON
            _arg_DEPENDS "${_arg_PLUGIN_DEPENDS}"
            _arg_VERSION "${_arg_VERSION}"
            BUILD_RPATH "${_PLUGIN_RPATH}"
            INSTALL_RPATH "${_PLUGIN_RPATH}"
            LIBRARY_OUTPUT_DIRECTORY "${_output_binary_dir}/${plugin_dir}"
            ARCHIVE_OUTPUT_DIRECTORY "${_output_binary_dir}/${plugin_dir}"
            RUNTIME_OUTPUT_DIRECTORY "${_output_binary_dir}/${plugin_dir}"
            OUTPUT_NAME "${name}"
            QT_SKIP_TRANSLATION "${skip_translation}"
            ${_arg_PROPERTIES}
            )
    append_extra_translations("${target_name}" "${_arg_EXTRA_TRANSLATIONS}")
    enable_pch(${target_name})

    foreach(file IN LISTS _arg_EXPLICIT_MOC)
        set_explicit_moc(${target_name} "${file}")
    endforeach()

    if (NOT _arg_SKIP_INSTALL)
        install(TARGETS ${target_name}
                EXPORT ${CSB_CASED_ID}
                RUNTIME DESTINATION "${plugin_dir}" OPTIONAL
                LIBRARY DESTINATION "${plugin_dir}" OPTIONAL
                ARCHIVE
                DESTINATION "${plugin_dir}"
                COMPONENT Devel EXCLUDE_FROM_ALL
                OPTIONAL
                )
    endif()
endfunction()

function(csb_extend_target target_name)
    cmake_parse_arguments(_arg
            ""
            "SOURCES_PREFIX;FEATURE_INFO"
            "LIBPATH;CONDITION;DEPENDS;PUBLIC_DEPENDS;DEFINES;PUBLIC_DEFINES;INCLUDES;PUBLIC_INCLUDES;SOURCES;EXPLICIT_MOC"
            ${ARGN}
            )

    if (${_arg_UNPARSED_ARGUMENTS})
        message(FATAL_ERROR "csb_extend_target had unparsed arguments")
    endif()

    condition_info(_extra_text _arg_CONDITION)
    if (NOT _arg_CONDITION)
        set(_arg_CONDITION ON)
    endif()
    if (${_arg_CONDITION})
        set(_feature_enabled ON)
    else()
        set(_feature_enabled OFF)
    endif()
    if (_arg_FEATURE_INFO)
        add_feature_info(${_arg_FEATURE_INFO} _feature_enabled "${_extra_text}")
    endif()
    if (NOT _feature_enabled)
        return()
    endif()

    csb_add_depends(${target_name}
            PRIVATE ${_arg_DEPENDS}
            PUBLIC ${_arg_PUBLIC_DEPENDS}
            )
    target_compile_definitions(${target_name}
            PRIVATE ${_arg_DEFINES}
            PUBLIC ${_arg_PUBLIC_DEFINES}
            )
    target_include_directories(${target_name} PRIVATE ${_arg_INCLUDES})

    set_public_includes(${target_name} "${_arg_PUBLIC_INCLUDES}")

    if (_arg_SOURCES_PREFIX)
        foreach(source IN LISTS _arg_SOURCES)
            list(APPEND prefixed_sources "${_arg_SOURCES_PREFIX}/${source}")
        endforeach()

        if (NOT IS_ABSOLUTE ${_arg_SOURCES_PREFIX})
            set(_arg_SOURCES_PREFIX "${CMAKE_CURRENT_SOURCE_DIR}/${_arg_SOURCES_PREFIX}")
        endif()
        target_include_directories(${target_name} PUBLIC $<BUILD_INTERFACE:${_arg_SOURCES_PREFIX}>)

        set(_arg_SOURCES ${prefixed_sources})
    endif()
    target_sources(${target_name} PRIVATE ${_arg_SOURCES})

    if (APPLE AND BUILD_WITH_PCH)
        foreach(source IN LISTS _arg_SOURCES)
            if (source MATCHES "^.*\.mm$")
                set_source_files_properties(${source} PROPERTIES SKIP_PRECOMPILE_HEADERS ON)
            endif()
        endforeach()
    endif()

    set_public_headers(${target_name} "${_arg_SOURCES}")

    foreach(file IN LISTS _arg_EXPLICIT_MOC)
        set_explicit_moc(${target_name} "${file}")
    endforeach()

endfunction()

function(csb_extend_plugin target_name)
    csb_plugin_enabled(_plugin_enabled ${target_name})
    if (NOT _plugin_enabled)
        return()
    endif()

    csb_extend_target(${target_name} ${ARGN})
endfunction()

function(csb_add_executable name)
    cmake_parse_arguments(_arg "SKIP_INSTALL;SKIP_TRANSLATION"
            "DESTINATION"
            "LIBPATH;DEFINES;DEPENDS;EXTRA_TRANSLATIONS;INCLUDES;SOURCES;PROPERTIES" ${ARGN})

    if ($_arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "csb_add_executable had unparsed arguments!")
    endif()

    update_cached_list(__CSB_EXECUTABLES "${name}")

    string(TOUPPER "BUILD_EXECUTABLE_${name}" _build_executable_var)
    set(_build_executable_default "ON")
    if (DEFINED ENV{CSB_${_build_executable_var}})
        set(_build_executable_default "$ENV{CSB_${_build_executable_var}}")
    endif()
    set(${_build_executable_var} "${_build_executable_default}" CACHE BOOL "Build executable ${name}.")

    if (NOT ${_build_executable_var})
        return()
    endif()

    set(_DESTINATION "${CSB_LIBEXEC_PATH}")
    if (_arg_DESTINATION)
        set(_DESTINATION "${_arg_DESTINATION}")
    endif()

    set(_EXECUTABLE_PATH "${_DESTINATION}")
    if (APPLE)
        # path of executable might be inside app bundle instead of DESTINATION directly
        cmake_parse_arguments(_prop "" "MACOSX_BUNDLE;OUTPUT_NAME" "" "${_arg_PROPERTIES}")
        if (_prop_MACOSX_BUNDLE)
            set(_BUNDLE_NAME "${name}")
            if (_prop_OUTPUT_NAME)
                set(_BUNDLE_NAME "${_prop_OUTPUT_NAME}")
            endif()
            set(_EXECUTABLE_PATH "${_DESTINATION}/${_BUNDLE_NAME}.app/Contents/MacOS")
        endif()
    endif()

    file(RELATIVE_PATH _RELATIVE_LIB_PATH "/${_EXECUTABLE_PATH}" "/${CSB_LIBRARY_PATH}")


    add_executable("${name}" ${_arg_SOURCES})

    if (_arg_LIBPATH)
        message("${name} LIBPATH: ${_arg_LIBPATH}")
        target_link_directories("${name}" PRIVATE ${_arg_LIBPATH})
    endif()

    target_include_directories("${name}" PRIVATE "${CMAKE_BINARY_DIR}/src" ${_arg_INCLUDES})
    target_compile_definitions("${name}" PRIVATE ${_arg_DEFINES} ${TEST_DEFINES} ${DEFAULT_DEFINES})
    target_link_libraries("${name}" PRIVATE ${_arg_DEPENDS} ${IMPLICIT_DEPENDS})


    set(skip_translation OFF)
    if (_arg_SKIP_TRANSLATION)
        set(skip_translation ON)
    endif()

    csb_output_binary_dir(_output_binary_dir)
    set_target_properties("${name}" PROPERTIES
            BUILD_RPATH "${_RPATH_BASE}/${_RELATIVE_LIB_PATH}"
            INSTALL_RPATH "${_RPATH_BASE}/${_RELATIVE_LIB_PATH}"
            RUNTIME_OUTPUT_DIRECTORY "${_output_binary_dir}/${_DESTINATION}"
            QT_SKIP_TRANSLATION "${skip_translation}"
            CXX_VISIBILITY_PRESET hidden
            VISIBILITY_INLINES_HIDDEN ON
            ${_arg_PROPERTIES}
            )
    append_extra_translations("${name}" "${_arg_EXTRA_TRANSLATIONS}")
    enable_pch(${name})

    if (NOT _arg_SKIP_INSTALL)
        install(TARGETS ${name} DESTINATION "${_DESTINATION}" OPTIONAL)
    endif()
endfunction()

function(csb_extend_executable name)
    if (NOT (name IN_LIST __CSB_EXECUTABLES))
        message(FATAL_ERROR "csb_extend_executable: Unknown executable target \"${name}\"")
    endif()
    if (TARGET ${name})
        csb_extend_target(${name} ${ARGN})
    endif()
endfunction()

function(csb_add_test name)
    cmake_parse_arguments(_arg "GTEST" "" "DEFINES;DEPENDS;INCLUDES;SOURCES" ${ARGN})

    if ($_arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "csb_add_test had unparsed arguments!")
    endif()

    update_cached_list(__CSB_TESTS "${name}")

    set(TEST_DEFINES SRCDIR="${CMAKE_CURRENT_SOURCE_DIR}")
    file(RELATIVE_PATH _RPATH "/${CSB_BIN_PATH}" "/${CSB_LIBRARY_PATH}")

    add_executable(${name} ${_arg_SOURCES})

    csb_add_depends(${name}
            PRIVATE ${_arg_DEPENDS} ${IMPLICIT_DEPENDS}
            )

    target_include_directories(${name} PRIVATE "${CMAKE_BINARY_DIR}/src" ${_arg_INCLUDES})
    target_compile_definitions(${name} PRIVATE ${_arg_DEFINES} ${TEST_DEFINES} ${DEFAULT_DEFINES})

    set_target_properties(${name} PROPERTIES
            BUILD_RPATH "${_RPATH_BASE}/${_RPATH}"
            INSTALL_RPATH "${_RPATH_BASE}/${_RPATH}"
            )
    enable_pch(${name})

    if (NOT _arg_GTEST)
        add_test(NAME ${name} COMMAND ${name})
        finalize_test_setup(${name})
    endif()
endfunction()

function(finalize_csb_gtest test_name)
    get_target_property(test_sources ${test_name} SOURCES)
    include(GoogleTest)
    gtest_add_tests(TARGET ${test_name} SOURCES ${test_sources} TEST_LIST test_list)

    foreach(test IN LISTS test_list)
        finalize_test_setup(${test})
    endforeach()
endfunction()

function(csb_map_source _VARNAME)
    cmake_parse_arguments(_arg "" "" "SOURCE_ROOT;SOURCES" ${ARGN})
    if ($_arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "csb_map_source had unparsed arguments!")
    endif()
    foreach (_arg_SOURCE IN ITEMS ${_arg_SOURCES})
        IF (NOT EXISTS ${_arg_SOURCE_ROOT}/${_arg_SOURCE})
            MESSAGE(FATAL_ERROR "File Not Found: ${_arg_SOURCE}")
        ELSE()
            LIST(APPEND _mappedSources ${_arg_SOURCE_ROOT}/${_arg_SOURCE})
        ENDIF()
    endforeach()
    SET("${_VARNAME}" ${_mappedSources} PARENT_SCOPE)
endfunction(csb_map_source)

function(csb_add_protocol)
    cmake_parse_arguments(_arg "" "" "LIBPATH;OUTPUT;SOURCES" ${ARGN})
    if ($_arg_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "csb_map_source had unparsed arguments!")
    endif()

    # Find Protobuf command
    find_program(PROTOBUF_PROTOC NAMES protoc)
    add_executable(proto::protoc_exec IMPORTED)
    set_target_properties(proto::protoc_exec PROPERTIES IMPORTED_LOCATION ${PROTOBUF_PROTOC})

    # Find gRPC CPP generator
    find_program(GRPC_CPP_PLUGIN NAMES grpc_cpp_plugin)
    mark_as_advanced(GRPC_CPP_PLUGIN)
    add_executable(proto::grpc_cpp_plugin IMPORTED)
    set_target_properties(proto::grpc_cpp_plugin PROPERTIES IMPORTED_LOCATION ${GRPC_CPP_PLUGIN})

    include_directories(${_arg_OUTPUT})

    foreach (_arg_SOURCE IN ITEMS ${_arg_SOURCES})
        GET_FILENAME_COMPONENT(ABSPATH ${_arg_SOURCE} ABSOLUTE)
        GET_FILENAME_COMPONENT(FILENAME_NOEXT ${ABSPATH} NAME_WE)
        IF (NOT EXISTS ${ABSPATH})
            MESSAGE(FATAL_ERROR "No such protocol file: ${ABSPATH}")
        ENDIF()

        SET(CODEGEN_TARGET ${FILENAME_NOEXT}_CodeGen)

        ADD_CUSTOM_TARGET(${CODEGEN_TARGET} ALL
                COMMENT Generating protobuf and gRPC source code for ${_arg_SOURCE}
                VERBATIM)

        ADD_CUSTOM_COMMAND(TARGET ${CODEGEN_TARGET}
                COMMAND ${PROTOBUF_PROTOC} --cpp_out ${_arg_OUTPUT} -I${CMAKE_CURRENT_SOURCE_DIR} ${ABSPATH}
                COMMENT "Generate protobuf source code: ${ABSPATH}"
                VERBATIM)


        ADD_CUSTOM_COMMAND(TARGET ${CODEGEN_TARGET}
                COMMAND ${PROTOBUF_PROTOC} --grpc_out ${_arg_OUTPUT} --plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN} -I${CMAKE_CURRENT_SOURCE_DIR} ${ABSPATH}
                COMMENT "Generate grpc source code: ${ABSPATH}"
                VERBATIM)

        FILE(GLOB ${FILENAME_NOEXT}_SOURCES ${_arg_OUTPUT}/${FILENAME_NOEXT}*.pb.cc)
        FILE(GLOB ${FILENAME_NOEXT}_HEADERS ${_arg_OUTPUT}/${FILENAME_NOEXT}*.pb.h)

        csb_add_library(${FILENAME_NOEXT} STATIC
                DEPENDS grpc protobuf
                LIBPATH ${_arg_LIBPATH}
                SOURCES
                ${${FILENAME_NOEXT}_SOURCES}
                ${${FILENAME_NOEXT}_HEADERS})

    endforeach()

endfunction(csb_add_protocol)