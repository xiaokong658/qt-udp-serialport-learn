#BINARY_ARTIFACTS_BRANCH = master
#PROJECT_USER_FILE_EXTENSION = .user

set(CSB_VERSION "3.0.0")                            # The CensorBot version.
set(CSB_VERSION_COMPAT "3.0.0")                     # The CensorBot Compatibility version.
set(CSB_VERSION_DISPLAY "3.0.0-beta")               # The CensorBot display version.
set(CSB_COPYRIGHT_YEAR "2020")                        # The CensorBot current copyright year.

set(CSB_SETTINGSVARIANT "CensorBot")                  # The CensorBot settings variation.
set(CSB_COPY_SETTINGSVARIANT "Alpaca Cloud Team")                 # The CensorBot settings to initially import.
set(CSB_DISPLAY_NAME "无人机管理线检测")                    # The CensorBot display name.
set(CSB_ID "CensorBot")                               # The CensorBot id (no spaces, lowercase!)
set(CSB_CASED_ID "CensorBot")                         # The cased CensorBot id (no spaces!)
set(CSB_BUNDLE_IDENTIFIER "CensorBot.${CSB_ID}") # The macOS application bundle identifier.
