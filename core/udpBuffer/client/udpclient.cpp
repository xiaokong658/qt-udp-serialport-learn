//
// Created by yun on 2020/8/4.
//

#include "udpclient.h"

MyudpClient::MyudpClient(QObject *parent, int port) : QObject(parent)
{
    m_udpSocket = new QUdpSocket(this);
    connect(m_udpSocket, SIGNAL(readyRead()), this, SLOT(slotRead()));
    ip = QHostAddress("127.0.0.1");
    this->port = port;
    m_udpSocket->connectToHost(ip, port);
}

MyudpClient::~MyudpClient()
{

}

void MyudpClient::slotRead()
{
    while(m_udpSocket->bytesAvailable()>0)
    {
        int n = m_udpSocket->bytesAvailable();  //接收到的字节数
        char *buf = new char[n];
        m_udpSocket->read(buf, n);   //读取数据
        unpackData(buf, n);   //解包
        delete []buf;
    }
}

//解包之后的处理
void MyudpClient::recv(char *data)
{
    printf("%s\n", data);
}

void MyudpClient::send(char *data) {
    packData(data, strlen(data));
}
