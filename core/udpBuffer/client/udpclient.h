//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_udpCLIENT_H
#define QT_UDP__LEARN_udpCLIENT_H


#include <QUdpSocket>
#include <QtNetwork>
#include "netcom.h"

class MyudpClient : public QObject, public NetComTemplate
{
Q_OBJECT

public:
    MyudpClient(QObject *parent, int port);
    ~MyudpClient();

    void recv(char *data);           //每解完一包之后的处理
    void send(char *data);

public slots:
    void slotRead();
};


#endif //QT_UDP__LEARN_udpCLIENT_H
