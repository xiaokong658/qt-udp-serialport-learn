//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_udpSERVER_H
#define QT_UDP__LEARN_udpSERVER_H

#include <QUdpSocket>
#include <QObject>
#include "netcom.h"

class MyudpServer : public QObject, public NetComTemplate
{
Q_OBJECT

public:
    MyudpServer(QObject *parent, int port);
    ~MyudpServer();

private slots:
    void slotRead();

protected:
    void recv(char *data);           //每解完一包之后的处理
};

#endif //QT_UDP__LEARN_udpSERVER_H
