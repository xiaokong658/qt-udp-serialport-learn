//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_NETCOM_H
#define QT_UDP__LEARN_NETCOM_H


#include <QUdpSocket>
#include <QDebug>
#include "databuffer.h"

#pragma pack(push, 1) //采用1字节对齐方式

//包头
typedef struct
{
    int nLen;  //包体长度
}PacketHead;

//封包对象：包头 + 包体
typedef struct
{
    PacketHead head;  //包头
    char *body;       //包体
}Packet;

#pragma pack(pop)

class NetComTemplate
{
public:
    QUdpSocket *m_udpSocket;  //通信套接字
    QHostAddress ip;
    quint16 port;
    DataBuffer m_Buffer;      //套接字关联的缓冲区

    void packData(char *data, int nLen);   //封包，发送
    void unpackData(char *data, int nLen); //将接收到的数据放在缓冲区后，解包
    virtual void recv(char *data);         //每解完一包之后的处理，留给继承的类去实现

    NetComTemplate();
    ~NetComTemplate();
};

#endif //QT_UDP__LEARN_NETCOM_H
