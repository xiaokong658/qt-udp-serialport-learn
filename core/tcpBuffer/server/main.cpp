//
// Created by yun on 2020/8/4.
//

#include <QCoreApplication>
#include <QApplication>
#include "tcpserver.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    MyTcpServer myTcpServer1(&a, 8088);

    return a.exec();
}