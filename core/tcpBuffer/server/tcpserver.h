//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_TCPSERVER_H
#define QT_UDP__LEARN_TCPSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QObject>
#include "netcom.h"

class MyTcpServer : public QTcpServer, public NetComTemplate
{
Q_OBJECT

public:
    MyTcpServer(QObject *parent, int port);
    ~MyTcpServer();

protected:
    void incomingConnection(qintptr socketDescriptor);
};

#endif //QT_UDP__LEARN_TCPSERVER_H
