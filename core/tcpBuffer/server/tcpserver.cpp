//
// Created by yun on 2020/8/4.
//

#include "tcpserver.h"
#include <QDataStream>
#include <stdlib.h>
#include <QDebug>

MyTcpServer::MyTcpServer(QObject *parent, int port): QTcpServer(parent)
{
    listen(QHostAddress("127.0.0.1"), port);
}

MyTcpServer::~MyTcpServer()
{

}

//出现一个新连接时调用
void MyTcpServer::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << "receive new connection" << socketDescriptor;
    m_tcpSocket = new QTcpSocket;
    m_tcpSocket->setSocketDescriptor(socketDescriptor);

    char *d1 = "data1";
    char *d2 = "data2";
    char *d3 = "data3";

    packData(d1, sizeof("data1"));   //封包，发送
    packData(d2, sizeof("data2"));
    packData(d3, sizeof("data3"));
}