//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_TCPCLIENT_H
#define QT_UDP__LEARN_TCPCLIENT_H


#include <QTcpSocket>
#include <QtNetwork>
#include "netcom.h"

class MyTcpClient : public QObject, public NetComTemplate
{
Q_OBJECT

public:
    MyTcpClient(QObject *parent, int port);
    ~MyTcpClient();

    void recv(char *data);           //每解完一包之后的处理

public slots:
    void slotRead();
};


#endif //QT_UDP__LEARN_TCPCLIENT_H
