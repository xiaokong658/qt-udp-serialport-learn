//
// Created by yun on 2020/8/4.
//

#include "tcpclient.h"

MyTcpClient::MyTcpClient(QObject *parent, int port) : QObject(parent)
{
    m_tcpSocket = new QTcpSocket;
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotRead()));
    m_tcpSocket->connectToHost(QHostAddress("127.0.0.1"), port);
}

MyTcpClient::~MyTcpClient()
{

}

void MyTcpClient::slotRead()
{
    while(m_tcpSocket->bytesAvailable()>0)
    {
        int n = m_tcpSocket->bytesAvailable();  //接收到的字节数
        char *buf = new char[n];
        m_tcpSocket->read(buf, n);   //读取数据
        unpackData(buf, n);   //解包
        delete []buf;
    }
}

//解包之后的处理
void MyTcpClient::recv(char *data)
{
    printf("%s\n", data);
}
