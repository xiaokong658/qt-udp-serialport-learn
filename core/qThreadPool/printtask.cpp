//
// Created by yun on 2020/8/4.
//

#include "printtask.h"
#include <QThread>
#include <iostream>

using std::cout;
using std::endl;

PrintTask::PrintTask() {

}
PrintTask::~PrintTask() noexcept {
    cout << "析构函数" << endl;
}

void PrintTask::run() {
    cout << "PrintTask run :" << QThread::currentThread() << endl;
}