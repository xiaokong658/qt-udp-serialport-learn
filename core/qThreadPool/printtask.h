//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_PRINTTASK_H
#define QT_UDP__LEARN_PRINTTASK_H

#include <QObject>
#include <QRunnable>

class PrintTask : public QObject, public QRunnable
{

    Q_OBJECT

public:
    PrintTask();
    // 析构函数 一般不需要写
    ~PrintTask();

protected:
    void run();

signals:

    //注意！要使用信号，采用QObejct 和 QRunnable多继承，记得QObject要放在前面
    void mySignal();
};

#endif //QT_UDP__LEARN_PRINTTASK_H
