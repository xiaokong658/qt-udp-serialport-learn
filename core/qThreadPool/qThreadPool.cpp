#include <QCoreApplication>
#include <QThreadPool>
#include "printtask.h"

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);
    QThreadPool pool;
    pool.setMaxThreadCount(3);

    for (int i = 0; i < 20; i++) {
        pool.start(new PrintTask());
    }

    return a.exec();
}