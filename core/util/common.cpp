//
// Created by yun on 2020/8/4.
//

#include "common.h"

QString common::fillStrPre(QString *str, int count, QLatin1Char aChar) {
    return QString("%1").arg(str->toInt(), count, 10, aChar);
}