//
// Created by yun on 2020/8/4.
//

#ifndef QT_UDP__LEARN_COMMON_H
#define QT_UDP__LEARN_COMMON_H

#include <QString>
#include <QLatin1Char>

class common {

public:
    /**
     * 字符串前缀填充
     * @param str
     * @param aChar
     * @return
     */
    QString fillStrPre(QString *str, int count, QLatin1Char aChar);
};


#endif //QT_UDP__LEARN_COMMON_H
